ZAZ BarCode

Extension for insert barcode in LibreOffice documents, with Python, of course.

Barcodes provided

* code39
* code128
* ean
* ean13
* ean8
* gs1
* gtin
* isbn
* isbn10
* isbn13
* issn
* jan
* pzn
* upc
* upca
* qrcode


Requirements:

LibreOffice 6.2+ with support for Python macros


Thanks!

https://gitlab.com/mauriciobaeza/zaz

https://github.com/WhyNotHugo/python-barcode

https://github.com/lincolnloop/python-qrcode


### Software libre, no gratis


This extension have a cost of maintenance of 1 euro every year.

BCH: `qztd3l00xle5tffdqvh2snvadkuau2ml0uqm4n875d`

BTC: `3FhiXcXmAesmQzrNEngjHFnvaJRhU1AGWV`


* [Look the wiki](https://gitlab.com/mauriciobaeza/zaz-barcode/wikis/home)
* [Mira la wiki](https://gitlab.com/mauriciobaeza/zaz-barcode/wikis/home_es)


Thanks for translations:

Esperanto: [Guillermo Molleda](https://gitlab.com/gmolleda)
